import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddEditDocumentComponent} from "../../../../libs/erp/documents/src/lib/add-edit-document/add-edit-document.component";
import {DocumentsListComponent} from "../../../../libs/erp/documents/src/lib/documents-list/documents-list.component";


const routes: Routes = [
  {path: 'add', component: AddEditDocumentComponent},
  {path: '', component: DocumentsListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
