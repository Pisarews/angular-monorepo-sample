import { Component } from '@angular/core';

@Component({
  selector: 'meritus-cloud-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'erp';
}
