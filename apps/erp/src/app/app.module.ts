import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from "./app-routing.module";
import {ErpDocumentsModule} from "@meritus-cloud/erp/documents";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BrowserAnimationsModule, AppRoutingModule, ErpDocumentsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
