module.exports = {
  projects: [
    '<rootDir>/apps/erp',
    '<rootDir>/apps/wms',
    '<rootDir>/libs/shared-ui-lib',
    '<rootDir>/libs/shared-cloud',
    '<rootDir>/libs/shared-erp',
    '<rootDir>/libs/shared-wms',
    '<rootDir>/libs/shared/ui',
    '<rootDir>/libs/erp/documents',
  ],
};
