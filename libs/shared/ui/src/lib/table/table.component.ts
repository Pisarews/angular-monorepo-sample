import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'meritus-cloud-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit {
  @Input() displayedColumns: string[];
  @Input() dataSource: any[];

  constructor() { }

  ngOnInit(): void {
  }

}
