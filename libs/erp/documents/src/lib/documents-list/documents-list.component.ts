import {Component, OnInit} from '@angular/core';
import {DocumentsServiceService} from "../api/documents-service.service";

@Component({
  selector: 'meritus-cloud-documents-list',
  templateUrl: './documents-list.component.html',
  styleUrls: ['./documents-list.component.scss']
})
export class DocumentsListComponent implements OnInit {

  dataSource = this.documentsService.getDocuments;
  displayColumns = ['position', 'name', 'weight', 'symbol'];

  constructor(private documentsService: DocumentsServiceService) {
  }

  ngOnInit(): void {
  }

}
