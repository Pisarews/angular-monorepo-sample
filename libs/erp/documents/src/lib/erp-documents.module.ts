import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DocumentsListComponent} from './documents-list/documents-list.component';
import {SharedUiModule} from "@meritus-cloud/shared/ui";
import {DocumentsServiceService} from "./api/documents-service.service";
import {AddEditDocumentComponent} from './add-edit-document/add-edit-document.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

@NgModule({
  imports: [CommonModule, SharedUiModule, MatFormFieldModule, MatInputModule],
  declarations: [
    DocumentsListComponent,
    AddEditDocumentComponent
  ],
  exports: [
    DocumentsListComponent,
    AddEditDocumentComponent
  ],
  providers: [DocumentsServiceService]
})
export class ErpDocumentsModule {
}
