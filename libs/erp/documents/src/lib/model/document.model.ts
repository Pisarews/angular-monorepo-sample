export interface Document {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
